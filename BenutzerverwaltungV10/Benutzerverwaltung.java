import java.io.*;
import java.util.Date;
import java.util.Scanner;
import java.util.Arrays;
//import java.io.Console;

public class Benutzerverwaltung {

    public static void main(String[] args) {
        try {
            BenutzerverwaltungV10.start();
        } catch (Exception e) {
            System.out.println(e.toString());
        }
    }
}

class BenutzerverwaltungV10{
    public static void start() throws Exception{
        BenutzerListe benutzerListe = new BenutzerListe();

        try{
            ObjectInputStream benutzerdatei = new ObjectInputStream(new FileInputStream("Benutzer.bin"));
            benutzerListe = (BenutzerListe)benutzerdatei.readObject();
            benutzerdatei.close();
        } catch (Exception e) {
            System.out.println(e);
            System.out.println("Lege Datei neu an");
        }


        Scanner tastatur = new Scanner(System.in);
        int chosenOption = 0;
        boolean correctInput = false;

        System.out.print("Willkommen bei Felix Huhn Systems™️\nBitte wählen Sie aus folgenden Optionen:\n - Anmelden: 1\n - Registrieren: 2\nAuswahl: ");
        chosenOption = tastatur.nextInt();
        while(!correctInput){
            if (chosenOption == 1) {
                correctInput = true;
                benutzerAnmelden(benutzerListe);
            } else if (chosenOption == 2) {
                correctInput = true;
                Benutzer newUser = benutzerRegistrieren();
                benutzerListe.insert(newUser);
            } else {
                System.err.print("Eingabe nicht erkannt, bitte wiederholen: ");
                chosenOption = tastatur.nextInt();
            }
            tastatur.close();
        }

        System.out.println(benutzerListe.select());
        ObjectOutputStream benutzerdatei1 = new ObjectOutputStream(new FileOutputStream("Benutzer.bin"));
        benutzerdatei1.writeObject(benutzerListe);
        benutzerdatei1.close();
    }

    private static Benutzer benutzerRegistrieren() {
        Scanner tastatur = new Scanner(System.in);
        boolean stringMatch = false;
        String username, password, confirmedPassword;
        Benutzer newUser = new Benutzer("", "");

        System.out.println("\nBenutzer registrieren ✏️");

        while (!stringMatch){
            System.out.print("Benutzername: ");
            username = tastatur.next();
            System.out.print("Passwort: ");
            password = tastatur.next();
            System.out.print("Passwort wiederholen: ");
            confirmedPassword = tastatur.next();
            
            if(password.equals(confirmedPassword)) {
                stringMatch = true;
                newUser = new Benutzer(username, password);
                tastatur.close();
            } else {
                System.err.println("Die Passwörter haben nicht übereingestimmt. Bitte wiederholen.");
            }
        }

        return newUser;      
    }

    private static boolean benutzerAnmelden(BenutzerListe userList){
        Scanner tastatur = new Scanner(System.in);
        int retryCount = 3;

        System.out.println("\nBenutzer anmelden 🔒");
        System.out.print("Benutzername: ");
        String username = tastatur.next();
        Benutzer user = userList.select(username);
        char[] userPassword = user.getPasswort();
    
        while (retryCount > 0) {
            System.out.print("Passwort: ");
            char[] inputPassword = Crypto.encrypt(tastatur.next().toCharArray());
            if (Arrays.equals(userPassword, inputPassword)){
                System.out.println("Sie sind angemeldet! ✔️");
                System.out.println("Letzte Anmeldung: " + user.getLastLoginDate().toString());
                System.out.println("Fehlversuche seit letzter Anmeldung: " + user.getUnsuccessfulLogins());
                user.setUnsuccessfulLogins(0);
                user.setLastLoginDate(new Date());
                tastatur.close();
                return true;
            }
            System.err.println("Nutzername und Passwort stimmen nicht überein. Bitte erneut versuchen!");
            user.increaseUnsuccessfulLogins();
            retryCount --;
        }
        System.out.println("Maximale Anzahl an Fehlversuchen erreicht.");
        tastatur.close();
        return false;
    }
}

class BenutzerListe implements Serializable {
    private Benutzer first;
    private Benutzer last;
    public BenutzerListe(){
        first = last = null;
    }
    public void insert(Benutzer b){
        // Sicherheitshalber setzen wir
        // den Nachfolger auf null:
        b.setNext(null);
        if(first == null){
            first = last = b;
        }
        else{
            last.setNext(b);
            last = b;
        }
    }
    public String select(){
        String s = "";
        Benutzer b = first;
        while(b != null){
            s += b.toString() + '\n';
            b = b.getNext();
        }
        return s;
    }
    public Benutzer select(String name){
        //geändert um Benutzerobjekt zu returnen. So lässt sich direkt mit den Benutzerobjekten interagieren und es muss nicht erst mit Strings agiert werden.
        Benutzer b = first;
        while(b != null){
            if(b.hasName(name)){
                return b;
            }
            b = b.getNext();
        }
        throw new Error("Benutzer nicht gefunden.");
    }

    public boolean delete(String name){
        Benutzer currentUser = first;
        Benutzer nextUser;
        
        while(currentUser != null){

            if (currentUser.hasName(name)){
                if(currentUser.getNext() == null){
                    first = null;
                    last = null;
                    return true;
                } else {
                    first = currentUser.getNext();
                    currentUser = null;
                    return true;
                }
            }

            nextUser = currentUser.getNext();

            if(nextUser != null && nextUser.hasName(name)){
                if(nextUser.getNext() == null){
                    nextUser = null;
                    currentUser.setNext(null);
                    last = currentUser;
                    return true;
                } else {
                    currentUser.setNext(nextUser.getNext());
                    nextUser = null;
                    return true;
                }
            }
            
            currentUser = nextUser;
        }
        
        return false;
    }
}

class Benutzer implements Serializable{
    private String name;
    private char[] passwort;
    private int unsuccessfulLogins;
    private Date lastLoginDate;

    private Benutzer next;

    public Benutzer(String name, String pw){
        this.name = name;
        this.passwort = Crypto.encrypt(pw.toCharArray());
        this.setUnsuccessfulLogins(0);
        this.setLastLoginDate(null);

        this.next = null;
    }


    public String getName(){
        return this.name;
    }

    public char[] getPasswort(){
        //So niemals im echten Leben machen :)
        return this.passwort;
    }

    public int getUnsuccessfulLogins() {
        return unsuccessfulLogins;
    }

    public void setUnsuccessfulLogins(int unsuccessfulLogins) {
        this.unsuccessfulLogins = unsuccessfulLogins;
    }

    public void increaseUnsuccessfulLogins(){
        this.unsuccessfulLogins++;
    }

    public String getLastLoginDate() {
        if (this.lastLoginDate == null) {
            return "niemals";
        }
        return lastLoginDate.toString();
    }

    public void setLastLoginDate(Date lastLoginDate) {
        this.lastLoginDate = lastLoginDate;
    }

    public Benutzer getNext(){
        return this.next;
    }

    public void setNext(Benutzer b){
        next = b;
    }

    public boolean hasName(String name){
        return name.equals(this.name);
    }

    public String toString(){
        String s = "";
        s += name + " ";
        s += passwort;
        return s;
    }

}

class Crypto {
    private static int cryptoKey = 65500;

    public static char[] encrypt(char[] s) {
        char[] encrypted = new char[s.length];
        for(int i = 0; i < s.length; i++) {
            encrypted[i] = (char)((s[i] + cryptoKey) % 128);
        }
        return encrypted;
    }

    public static char[] decrypt(char[] s) {
        char[] decrypted = new char[s.length];
        for(int i = 0; i < s.length; i++) {
            //Math.floorMod statt % da % negative Ergebnisse zurückliefert, welche dann evtl erst umgewandelt werden müssten.
            decrypted[i] = (char)Math.floorMod(s[i] - cryptoKey, 128);
        }
        return decrypted;
    }
}
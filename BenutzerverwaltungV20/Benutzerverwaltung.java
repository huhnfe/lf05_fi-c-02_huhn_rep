import java.io.*;
import java.util.Arrays;
import java.util.Scanner;

public class Benutzerverwaltung {

    public static void main(String[] args) {
            BenutzerverwaltungV20.start();
    }
}

class BenutzerverwaltungV20{
    private static BenutzerListe benutzerListe;

    public static void start() {
        benutzerListe = new BenutzerListe();

        benutzerListe.insert(new Benutzer("Paula", "paula"));
        benutzerListe.insert(new Benutzer("Adam37", "adam37"));
        benutzerListe.insert(new Benutzer("Darko", "darko"));
        //Das Passwort des Nutzers kann mit Crypto.decrypt() entschlüsselt werden und ist Hallo123
        //benutzerListe.insert(new Benutzer("Admin", new char[]{36, 61, 72, 72, 75, 13, 14, 15}));

        // Hier bitte das Menü mit der Auswahl
        //  - Anmelden
        //  - Registrieren
        // einfügen, sowie die entsprechenden Abläufe:
        // Beim Registrieren 2x das Passwort einlesen und vergleichen,
        // das neue Benutzerobjekt erzeugen und in die Liste einfügen.
        // Beim Anmelden (max. 3 Versuche) name und passwort einlesen,
        // in der Liste nach dem Namen suchen und das eingegebene Passwort
        // mit dem gespeicherten vergleichen.

        Scanner tastatur = new Scanner(System.in);
        boolean systemLäuft = true;
        while(systemLäuft) {
            // Hier meldet sich der Benutzer an, z.B. Paula mit ihrem Passwort paula:
            // Versuchen Sie sich als Benutzer Admin anzumelden, indem Sie das Passwort knacken.
            System.out.print("Name: ");
            String inputName = tastatur.next();
            System.out.print("Passwort: ");
            String inputPasswort = tastatur.next();
            if (authenticate(inputName, Crypto.encrypt(inputPasswort.toCharArray()))) {
                System.out.println("Hallo " + inputName + "! Sie sind angemeldet.");
                // Arbeitsumgebung des Benutzers starten.
                // ...
                // Benutzer hat sich abgemeldet.
                System.out.println("Auf Wiedersehen.");
                System.out.print("System herunterfahren? [j/n] ");
                systemLäuft = !tastatur.next().equals("j");
            } else {
                System.out.println("Name oder Passwort falsch.");
            }
        }
    }

    public static boolean authenticate(String name, char[] cryptoPw) {
        Benutzer b = benutzerListe.getBenutzer(name);
        if(b != null) {
            if(b.hasPasswort(cryptoPw)){
                return true;
            }
        }
        return false;
    }
}

class BenutzerListe {
    private Benutzer first;
    private Benutzer last;
    public BenutzerListe(){
        first = last = null;
    }

    public Benutzer getBenutzer(String name) {
        Benutzer b = first;
        while(b != null){
            if(b.hasName(name)) {
                return b;
            }
            b = b.getNext();
        }
        return null;
    }

    public void insert(Benutzer b) {
        // Sicherheitshalber setzen wir
        // den Nachfolger auf null:
        b.setNext(null);
        if(first == null) {
            first = last = b;
        }
        else {
            last.setNext(b);
            last = b;
        }
    }

    public String select() {
        String s = "";
        Benutzer b = first;
        while(b != null){
            s += b.toString() + '\n';
            b = b.getNext();
        }
        return s;
    }

    public String select(String name) {
        Benutzer b = first;
        while(b != null){
            if(b.hasName(name)) {
                return b.toString();
            }
            b = b.getNext();
        }
        return "";
    }

    public boolean delete(String name) {
        // ...
        return true;
    }
}

class Benutzer {
    private String name;
    private char[] passwort;  // Verschlüsselt!

    private Benutzer next;

    public Benutzer(String name, String pw){
        this.name = name;
        this.passwort = Crypto.encrypt(pw.toCharArray());

        this.next = null;
    }

    public boolean hasName(String name){
        return name.equals(this.name);
    }

    public boolean hasPasswort(char[] cryptoPw){
        return Arrays.equals(this.passwort, cryptoPw);
    }

    public String toString(){
        String s = "";
        s += name + " ";
        s += passwort;
        return s;
    }

    public Benutzer getNext(){
        return next;
    }

    public void setNext(Benutzer b){
        next = b;
    }
}

class Crypto {
    private static int cryptoKey = 65500;

    public static char[] encrypt(char[] s) {
        char[] encrypted = new char[s.length];
        for(int i = 0; i < s.length; i++) {
            encrypted[i] = (char)((s[i] + cryptoKey) % 128);
        }
        return encrypted;
    }

    public static char[] decrypt(char[] s) {
        char[] decrypted = new char[s.length];
        for(int i = 0; i < s.length; i++) {
            //Math.floorMod statt % da % negative Ergebnisse zurückliefert, welche dann evtl erst umgewandelt werden müssten.
            decrypted[i] = (char)Math.floorMod(s[i] - cryptoKey, 128);
        }
        return decrypted;
    }
}
/**
  *   Aufgabe:  Recherechieren Sie im Internet !
  * 
  *   Sie dürfen nicht die Namen der Variablen verändern !!!
  *
  *   Vergessen Sie nicht den richtigen Datentyp !!
  *
  *
  * @version 1.0 from 21.08.2019
  * @author << Felix Huhn >>
  */

public class WeltDerZahlen {

    public static void main(String[] args) {
      
      /*  *********************************************************
      
           Zuerst werden die Variablen mit den Werten festgelegt!
      
      *********************************************************** */
      // Im Internet gefunden ?
      // Die Anzahl der Planeten in unserem Sonnesystem                    
      int anzahlPlaneten =  8;
      
      // Anzahl der Sterne in unserer Milchstraße
        double  anzahlSterne = 1.5E11;
      
      // Wie viele Einwohner hat Berlin?
        int  bewohnerBerlin = 3769000;
      
      // Wie alt bist du?  Wie viele Tage sind das?
      
        int alterTage = 8352;
      
      // Wie viel wiegt das schwerste Tier der Welt?
      // Schreiben Sie das Gewicht in Kilogramm auf!
        int gewichtKilogramm = 190000;
      
      // Schreiben Sie auf, wie viele km² das größte Land er Erde hat?
        double flaecheGroessteLand = 1.71E7;
      
      // Wie groß ist das kleinste Land der Erde?
      
        double flaecheKleinsteLand = 4.4E-1;
      
      
      
      
      /*  *********************************************************
      
           Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
      
      *********************************************************** */
      
      System.out.println("Anzahl der Planeten: " + anzahlPlaneten);

      System.out.println("Anzahl der Sterne: " + anzahlSterne);

      System.out.println("Anzahl der Bewohner Berlin: " + bewohnerBerlin);

      System.out.println("Alter in Tagen: " + alterTage);

      System.out.println("Gewicht des schwersten Tiers der Welt in Kilogramm: " + gewichtKilogramm);

      System.out.println("Fläche des größten Landes der Welt in km²: " + flaecheGroessteLand);

      System.out.println("Fläche des kleinsten Landes der Welt in km²: " + flaecheKleinsteLand);
        
      System.out.println(" *******  Ende des Programms  ******* ");
      
    }
}
  
  
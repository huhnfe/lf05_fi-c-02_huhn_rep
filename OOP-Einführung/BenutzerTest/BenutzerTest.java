public class BenutzerTest {
    public static void main(String[] args) {
        Benutzer user1 = new Benutzer();
        System.out.println(user1.getName());
        user1.setName("Hans");
        System.out.println(user1.getName());

        Benutzer user2 = new Benutzer("Manfred", "Manni.Manfred@oszimt.de", "SupersicheresPasswort123");
        System.out.println(user2.getName());
        System.out.println(user2.getEmail());
    }
}

import java.lang.reflect.Array;
import java.util.Scanner;

class Fahrkartenautomat
{
    static Scanner tastatur = new Scanner(System.in);
    static String[] fahrkartenTypen = {"Einzelfahrschein Berlin AB", 
                                "Einzelfahrschein Berlin BC",
                                "Einzelfahrschein Berlin ABC",
                                "Kurzstrecke",
                                "Tageskarte Berlin AB",
                                "Tageskarte Berlin BC",
                                "Tageskarte Berlin ABC",
                                "Kleingruppen-Tageskarte Berlin AB",
                                "Kleingruppen-Tageskarte Berlin BC",
                                "Kleingruppen-Tageskarte Berlin ABC"};
    
    static double[] fahrkartenKosten = {2.9,
                                3.3,
                                3.6,
                                1.9,
                                8.6,
                                9.0,
                                9.6,
                                23.5,
                                24.3,
                                24.9};
    public static void main(String[] args)
    {
        boolean naechsteFahrkarte = true;
        while (naechsteFahrkarte) {
            System.out.println();
            double zuZahlen = fahrkartenbestellungErfassen();
            double returnMoney = fahrkartenBezahlen(zuZahlen);
            fahrkartenAusgeben();
            rueckgeldAusgeben(returnMoney);
            naechsteFahrkarte = nochEineFahrkarte();
        }
    }

    public static double fahrkartenbestellungErfassen() {
        for(int i = 0; i<fahrkartenTypen.length; i++){
            System.out.printf("%d\t%-40s%.2f€\n", i+1, fahrkartenTypen[i], fahrkartenKosten[i]);
        }
        System.out.print("Bitte Fahrkartenart wählen: ");
        int fahrkartenArt = tastatur.nextInt();
        while (fahrkartenArt < 1 || fahrkartenArt > fahrkartenKosten.length) {
            System.out.print("Bitte gültigen Wert eingeben: ");
            fahrkartenArt = tastatur.nextInt();
        }
        double zuZahlen = fahrkartenKosten[fahrkartenArt-1];
        System.out.print("Anzahl der Fahrkarten eingeben: ");
        int anzahlTickets = tastatur.nextInt();
        while (anzahlTickets < 1 || anzahlTickets > 10) {
            System.out.print("Bitte zwischen 1 und 10 Tickets auswählen: ");
            anzahlTickets = tastatur.nextInt();
        }
        return zuZahlen * anzahlTickets;
    }
    
    public static double fahrkartenBezahlen(double zuZahlen) {
        double eingeworfeneMünze;
        double eingezahlterGesamtbetrag = 0.0;
        while(eingezahlterGesamtbetrag < zuZahlen)
        {
            System.out.printf("Noch zu zahlen: %.2f Euro\n", (zuZahlen - eingezahlterGesamtbetrag));
            System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
            eingeworfeneMünze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfeneMünze;
        }
        return eingezahlterGesamtbetrag - zuZahlen;
    }

    public static void fahrkartenAusgeben() {
       // Fahrscheinausgabe
       // -----------------
       System.out.println("\nFahrschein wird ausgegeben");
       for (int i = 0; i < 8; i++)
       {
          System.out.print("=");
          try {
			Thread.sleep(250);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
       }
       System.out.println("\n\n");
    }

    public static void rueckgeldAusgeben(double rückgabebetrag) {
        // Rückgeldberechnung und -Ausgabe
        // -------------------------------
       if(rückgabebetrag > 0.0)
       {
    	   System.out.printf("Der Rückgabebetrag in Höhe von %.2f EURO\n",rückgabebetrag);
    	   System.out.println("wird in folgenden Münzen ausgezahlt:");

           while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
           {
        	  System.out.println("2 EURO");
	          rückgabebetrag -= 2.0;
           }
           while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
           {
        	  System.out.println("1 EURO");
	          rückgabebetrag -= 1.0;
           }
           while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
           {
        	  System.out.println("50 CENT");
	          rückgabebetrag -= 0.5;
           }
           while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
           {
        	  System.out.println("20 CENT");
 	          rückgabebetrag -= 0.2;
           }
           while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
           {
        	  System.out.println("10 CENT");
	          rückgabebetrag -= 0.1;
           }
           while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
           {
        	  System.out.println("5 CENT");
 	          rückgabebetrag -= 0.05;
           }
       }

       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir wünschen Ihnen eine gute Fahrt.\n");
    }

    public static boolean nochEineFahrkarte() {
        char antwort;
        System.out.print("möchten Sie noch eine Fahrkarte kaufen? (J/N): ");
        antwort = tastatur.next().charAt(0);
        while (true) {
            if (antwort == 'J' || antwort == 'j') {
                return true;
            } else if (antwort == 'N' || antwort == 'n') {
                return false;
            } else {
                System.out.print("Bitte mit J oder N antworten: ");
                antwort = tastatur.next().charAt(0);
            }
        }
    }
}
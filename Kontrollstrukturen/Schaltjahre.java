import java.util.Scanner;

public class Schaltjahre {
    public static void main(String[] args) {

        Scanner tastatur = new Scanner(System.in);
        System.out.print("Jahreszahl eingeben: ");
        int jahr = tastatur.nextInt();

        if (istSchaltjahr(jahr)) {
            System.out.printf("Das Jahr %d ist ein Schaltjahr\n", jahr);
        } else {
            System.out.printf("Das Jahr %d ist kein Schaltjahr\n", jahr);
        }

        tastatur.close();
    }

    public static boolean istSchaltjahr(int jahreszahl) {
        if (jahreszahl < -45 || jahreszahl%4 != 0 || (jahreszahl%100 == 0 && jahreszahl%400 != 0)){
            return false;
        } else {
            return true;
        }
    }
}

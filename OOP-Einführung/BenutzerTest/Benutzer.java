public class Benutzer {

    private String name;
    private String email;
    private String passwort;
    private String berechtigungsstatus;
    private boolean anmeldestatus;
    private long letzerLogin;

    //TODO: Konstruktoren

    public Benutzer(){
        setName("unknown");
        setEmail("unknown");
        setPasswort("passwort");
        setBerechtigungsstatus("keine Berechtigungen");
        setAnmeldestatus(false);
        setLetzerLogin(0);
    };

    public Benutzer(String name, String email, String passwort) {
        setName(name);
        setEmail(email);
        setPasswort(passwort);
        setBerechtigungsstatus("keine Berechtigungen");
        setAnmeldestatus(false);
        setLetzerLogin(0);
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName(){
        return this.name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    private void setPasswort(String passwort) {
        this.passwort = passwort;
    }

    private String getPasswort() {
        return this.passwort;
    }

    public boolean changePasswort(String altesPasswort, String neuesPasswort) {
        if(altesPasswort == getPasswort()) {
            setPasswort(neuesPasswort);
            return true;
        } else {
            return false;
        }
    }

    public void setBerechtigungsstatus(String berechtigungsstatus) {
        this.berechtigungsstatus = berechtigungsstatus;
    }

    public String getBerechtigungsstatus() {
        return berechtigungsstatus;
    }

    public void setAnmeldestatus(boolean anmeldestatus) {
        this.anmeldestatus = anmeldestatus;
    }

    public boolean getAnmeldestatus() {
        return anmeldestatus;
    }

    public void setLetzerLogin(long letzerLogin) {
        this.letzerLogin = letzerLogin;
    }

    public long getLetzerLogin() {
        return letzerLogin;
    }

}
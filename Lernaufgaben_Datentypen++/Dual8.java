import java.util.Scanner;

public class Dual8 {
    public static void main(String[] args) {
        Scanner myScanner = new Scanner(System.in);
        System.out.print("Bitte Dualzahl eingeben: ");
        String inputByte = myScanner.nextLine();
        int outputNumber = Integer.parseInt(inputByte,2);
        System.out.println("Die Dezimalzahl lautet " + outputNumber);
    }
}

import java.util.Scanner;

public class EndlessSum {
    public static void main(String[] args) {
        Scanner tastatur = new Scanner(System.in);
        System.out.print("Zahl eingeben bis zu der aufsummiert werden soll: ");
        System.out.println(SumA(tastatur.nextInt()));
        tastatur.close();
    }
    public static int SumA(int n) {
        int i = 0;
        int result = 0;
        while(i <= n) {
            result += i;
            i++;
        }
        return result;
    }

}

import java.util.Scanner;
public class Leistung{
    public static void main(String[] args) {
        Scanner myScanner = new Scanner(System.in);
        double einzelleistung;
        double gesamtleistung;
        double gesamtstromstaerke;
        final double netzspannung = 230.0;
        final double maxStromstaerke = 16.0;
        int anzahlPCs;
        int anzahlStromkreise;
        System.out.print("\nLeistung eines PC-Arbeitsplatzes [in Watt]: ");
        einzelleistung = myScanner.nextDouble();
        System.out.print("Anzahl der PC-Arbeitsplätze: ");
        anzahlPCs = myScanner.nextInt();
        // Berechnung der erforderlichen Stromstärke und der
        // Anzahl der benötigten Stromkreise:......
        gesamtleistung = einzelleistung * anzahlPCs;
        gesamtstromstaerke = gesamtleistung / netzspannung;
        anzahlStromkreise = (int) java.lang.Math.ceil(gesamtstromstaerke / maxStromstaerke);
        
        System.out.println("Gesamtleistung:  "+ gesamtleistung);
        System.out.println("Gesamtstromstärke:  "+ gesamtstromstaerke);
        System.out.println("Anzahl der Stromkreise:  "+ anzahlStromkreise);
    }
}
